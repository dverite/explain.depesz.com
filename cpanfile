# This file is used by cpanm.
# To install dependencies, run >> cpanm --local-lib ~/perl5 --installdeps . <<, or something similar, depending on your setup.
#
# More details:
# https://metacpan.org/pod/cpanfile
# https://metacpan.org/pod/cpanm

requires 'Carp';
requires 'Config';
requires 'DBI';
requires 'DBD::Pg';
requires 'Date::Simple';
requires 'Digest::MD5';
requires 'Email::Sender::Simple';
requires 'Email::Simple';
requires 'Email::Valid';
requires 'Encode';
requires 'English';
requires 'File::Spec';
requires 'Mojolicious';
requires 'Pg::Explain', '>= 2.1';
requires 'Number::Bytes::Human';
requires 'Pg::SQL::PrettyPrinter';

# vim: set ft=perl:
