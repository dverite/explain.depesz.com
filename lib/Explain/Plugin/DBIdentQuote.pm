package Explain::Plugin::DBIdentQuote;

use Number::Bytes::Human qw(format_bytes);
use Mojo::Base 'Mojolicious::Plugin';

sub register {
    my ( $self, $app ) = @_;

    # register helpers
    $app->helper( db_ident_quote => \&db_ident_quote );
}

sub db_ident_quote {
    my $self = shift;
    local $_ = shift;

    return $_ if /\A[a-z][a-z0-9_]*\z/;
    return sprintf '"%s"', $_;
}

1;
